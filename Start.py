#!/usr/bin/env python
# -*- coding: utf-8 -*-
# PYTHON_ARGCOMPLETE_OK



import argparse as ap
import argcomplete



if __name__ == '__main__':
  parser = ap.ArgumentParser()
  parser.add_argument('--server', nargs='?',metavar='',  help='Inits the Server')
  parser.add_argument('--test', nargs='?',metavar='',  help='Run tests inthe Server')
  parser.add_argument('--databaseClean', nargs='?',metavar='',  help='Cleans the database')
  parser.add_argument('--load', nargs='?',metavar='',  help='Creates a database')
  
  argcomplete.autocomplete(parser)
  args = parser.parse_args()

import sys
import traceback
mainFolder  = sys.path[0]
sys.path.insert(0, mainFolder+"/")



try:
    
    
    
    if len(sys.argv) > 1 and sys.argv[1] == "--server":
       import src.presentation.Server as server
       from Naked.toolshed.shell import execute_js
       success = execute_js('./config/parse-server/index.js&')
       server.run(8000)
    if len(sys.argv) > 1 and sys.argv[1] == "--test":
       import src.test.Suite as tests
       tests.run()
    
    if len(sys.argv) > 1 and sys.argv[1] == "--load":
       import src.test.Suite as suiteTest
       suiteTest.load()
       
    if len(sys.argv) > 1 and sys.argv[1] == "--databaseClean":
       from  src.data.index.CleanDatabase import CleanDatabase
       cleanDatabase = CleanDatabase()
       cleanDatabase.run()
       
    
    
except:
    traceback.print_exc()
    print "Error"
    pass




