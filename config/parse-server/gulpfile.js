var gulp = require("gulp"),
    gutil = require("gulp-util"),
    nodemon = require("gulp-nodemon");



//Task criada para executar o script com o nodemon
gulp.task("start", function() {
    nodemon({
        script: "index.js",
        ext: "js",
        ignore: ["./node_modules/**"]
    }).on("restart", function() {

        console.log("Restarting");
    });
});


gulp.task("default", ["start"]);
