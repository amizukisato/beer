var dotenv = require("dotenv"); 
dotenv.load();

var express = require("express");
var ParseServer = require("parse-server").ParseServer;
var ParseDashboard = require('parse-dashboard');
var http = require("http")
var fs = require("fs")
if (!databaseUri) {
    console.log("DATABASE_URI not specified, falling back to localhost.");
}
var properties = require ("properties");
var options = {
	  namespaces: true,
	  sections: true,
	  variables: true
};
var pathFile = "../config.properties"

if (!fs.existsSync(pathFile)) {
   pathFile = "./config/config.properties"
}



var properties_content = fs.readFileSync(pathFile, 'utf8');
var parseConfig = properties.parse (properties_content, options).Parse;
var integerFields = ["parsePort","dashbordPort"]

integerFields.forEach(field =>{
	if(field in parseConfig){
		
		parseConfig[field] = parseInt(parseConfig[field])
	}
	
})


var databaseUri = parseConfig.mongoUrl;


var api = new ParseServer({
    databaseURI: databaseUri || "mongodb://mongo:27017/cachaceiro",
    appId: parseConfig.parseAplicationId ,
    masterKey:parseConfig.parseMasterKey,
    //javascriptKey: parseConfig.parseRestApiKey,
    //clientKey: parseConfig.parseRestApiKey,
    serverURL: parseConfig.parseUrl
    
    
});

var app = express();

var mountPath = process.env.PARSE_MOUNT || "/parse";
app.use(mountPath, api);


var port = parseConfig.parsePort;
var httpServer = http.createServer(app);
httpServer.listen(port, function () {
    console.log("parse-server-example running on port " + port + ".");
});

// This will enable the Live Query real-time server
ParseServer.createLiveQueryServer(httpServer);



// Set up parse dashboard
var dashboard = new ParseDashboard({  
  "apps": [{
      "serverURL": parseConfig.parseDashURL,
      "appId": parseConfig.parseAplicationId,
      "masterKey": parseConfig.parseMasterKey,
      "appName": parseConfig.appName,
      "production": false
  }],
  "users": [
    {
      "user":"ailton",
      "pass":"99719797"
    },
    {
      "user": "cachaceiro",
      "pass": "devcachaceiroparsedashboard@0!6"
    }
  ],
  "iconsFolder": "icons"
}, true);

var dashApp = express();

// make the Parse Dashboard available at /dashboard
dashApp.use('/dashboard', dashboard);  

// Parse Server plays nicely with the rest of your web routes
dashApp.get('/', function(req, res) {  
  res.status(200).send('Parse Dashboard App');
});
var portDash = parseConfig.dashbordPort
var httpServerDash = http.createServer(dashApp);  
httpServerDash.listen(portDash, function() {  
    console.log('dashboard-server running on port '+portDash+".");
});
module.exports = app;
