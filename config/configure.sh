#!/bin/bash

if [ "$(id -u)" != "0" ]; then
	echo "This command only works with a sudoer."
	exit 1	
fi

sudo apt-get -y install python-pip
sudo apt-get -y install python-dev
sudo apt-get -y install python-setuptools
sudo apt-get -y install git
sudo -H pip install -U pip
sudo pip install requests_toolbelt
sudo pip install git+https://github.com/milesrichardson/ParsePy.git
sudo pip install requests
sudo pip install Flask
sudo pip install flask_restful
sudo pip install flask_httpauth
sudo pip install netifaces
sudo pip install -U flask-cors
sudo pip install netifaces
sudo pip install pymongo
sudo pip install python-dateutil
sudo pip install numpy
sudo pip install tensorflow
sudo pip install Naked
sudo pip install argcomplete
sudo activate-global-python-argcomplete


