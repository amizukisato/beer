**Beer Server**
----


* **Instalação**
		Instalar dependências python 2.7
		Nodejs 9x

		Existem algumas dependencias que estão em 
		/config/configure.sh 

		basta rodar sudo sh /config/configure.sh

		Verifique a pasta node_modules em config

		se necessario use:

		cd /config/parse-server
		npm install
	


* **Inicialização**


		Para iniciar o sistema na pasta raiz execute 
			python Start.py  --server
		
		Para carregar os dados iniciais. Com o servidor iniciado execute:
			python Start.py  --load
		
		Para limpar o banco de dados. Com o servidor iniciado execute:
			python Start.py  --databaseClean
		
		Acesse :
			http://seu IP:8000 //para acessar o exemplo
			http://localhost:4001/dashboard // para acessar o banco de dados 
				usuario: ailton
				senha: 99719797
			
	


* **URLs do servidor**
		/crud/user
		/crud/beer
		/crud/item

* **Parâmetros de URL**
	
`GET`

	className: "Obrigatório"
	objectId:  "Opcional"
		

- Retornar as informações da cerveja de  id  abcdefg

 		GET /crud/beer/abcdefg


- Retornar uma lista contento todas cachacas e suas informações	
	
		GET /crud/beer 
 		

- Retornar uma lista de nomes contendo cervejas de preço entre  [10 a 15]
	
		GET /crud/beer?fields=name&filter_by=price[gte]=10,price[lte]=15 
 
- Retornar uma lista de beer com o nome igual a SKOL
 
		GET /crud/beer?filter_by=name[eq]=SKOL
 
- Retornar as 10 primeiras beer e em seguida, as 10 próximas

		GET /crud/beer?page=init=1,end=10
		GET /crud/beer?page=init=11,end=20

- Retornar o nome e descrição das 20 primeiras cervejas com preco maior que 5 ordenadas pelo nome decrescentemente

		GET /crud/beer?page=init=1,end=20&fields=nome,description&filter_by=price[gt]=5&sort_by=-name

- Retornar todos os Ingredientes cadastrados no banco
	
		GET /crud/item?filter_by=classType[eq]=INGREDIENT
		
- Retornar todos os tipos de comida cadastrados no banco
	
		GET /crud/item?filter_by=classType[eq]=FOOD
		




- Retornar todas as cervejas que contenham MALTE em sua composição. Primeiro uma busca em item com o nome, em seguida sera passado o objectId para a url cerveja
		
		GET /crud/item?filter_by=name[eq]=MALTE
		var malteId = result['data']['objectId']
		
		GET /crud/beer?filter_by=ingredients[in]=malteId


- Retornar todas as cervejas juntamente com o valor de preço máximo e mínimo
		
		GET /crud/beer?function=max[price]&function=min[price]
		

	
- Observações
		
		filter_by, eq, neq, gte, gt, lte, lt, sort_by, in, search_by, fields, page, max, min,sum, function são palavras reservadas
		
		Queries com tipo double ou float serão arredondadas a 3 casas decimais
		
		

`POST`
	
	Campos: Obrigatório
	ClassName: "Obrigatório"
	
	
- Efetuar Login
		
		POST /crud/user/login
		
		
	
	* bodyparams	
```javascript

{
	"username":String, 
	"password": String
}
	
``` 


`DELETE`
		
		objectId : "Obrigatório"
		Exemplo:  'DELETE /crud/product/1234, remove as informações do produto 1234'	
...


`Esquema:`
Sobre a extensibilidade, para estender o sistema e adicionar mais campos por exemplo: basta alterar o arquivo Schema.json

```javascript
	{
	"user":{
            "table": "_User",
            "fields": {
                "name": "string",
                "email": "email",
                "password": "string",
                "username": "string"
            }
            
        },
        "item":{
            "table": "Item",
            "fields": {
                "name": "ln_string",
                "classType": "string"
            },
            "unique":["name"]
        },
        "beer":{
                "table": "Beer",
                "fields": {
                    "name": "string",
                    "price": "float",
                    "description":"ln_string",
                    "ingredients": ["pk_Item"], 
                    "gl": "float",
                    "minTemperature": "float",
                    "maxTemperature": "float",
                    "harmonization": ["pk_Item"],
                    "picture":"string",
                    "linkBuy": "string",
                    "history":"ln_string"
                },
                "unique":["name"]
          }
 
    
	    
}

```
  		
  		
  			

	
