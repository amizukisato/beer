#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 11:26:33 2018

@author: ailton
"""

import json
import src.utils.Properties as properties

class Schema:
    
    def __init__(self,sesionToken = None, language=None):
        self.sesionToken = sesionToken
        self.language = language
        mainFolder = properties.getInstance().getMainFolder()
        schemaFileName = mainFolder + 'src/data/parse/schema.json'
        schemaFile = open(schemaFileName, 'r').read()
        self.schema = json.loads(schemaFile)['schema']
        
        
    def getAll(self, args, returnJson = False):
        return self.schema
    
    def get(self, schemaName, returnJson = False):
        return self.schema[schemaName]
        
        