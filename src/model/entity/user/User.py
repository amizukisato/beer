#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 13:15:55 2018

@author: ailton
"""

from src.model.entity.crud.CRUD import CRUD
from src.data.User import UserData


class User(CRUD):
    def __init__(self, sessionToken = None, language="en"):
        super(User, self).__init__("user", None, language)
        self.userData = UserData(language)

    
    def insert(self, user, returnJson=False):
        return self.userData.register(user, returnJson)
        
    def login(self, userName, userPassword, returnJson=False):
        return self.userData.login(userName, userPassword, returnJson)
    
    def loginWithAuthData(self, authData, retornJSON=False):
        return self.userData.loginWithAuthData(authData, retornJSON)
    
        
   
        
        
        
        