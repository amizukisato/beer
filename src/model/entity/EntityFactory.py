#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 12:15:25 2018

@author: ailton
"""

from crud.CRUD import CRUD
from user.User import User
from schema.Schema import Schema


class EntityFactory:
    
    mapEntityClassName = {
        'user': User,
        'schema': Schema
    }
    
    def __init__(self, className, sessionToken=None, language="en"):
        self.className = className
        if className in self.mapEntityClassName:
            self.Entity = self.mapEntityClassName[className]
            self.Instance = self.Entity(sessionToken, language)
        else:
            self.Entity = CRUD
            self.Instance = self.Entity(self.className, sessionToken, language)
    
    def getEntityInstance(self):
        return self.Instance
    
    def getEntity(self):
        return self.Entity
        
    
    