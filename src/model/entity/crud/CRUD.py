#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 13:14:35 2018

@author: ailton
"""

from src.data.Database import Database



class CRUD(object):
    
    def __init__(self, className, sessionToken, language):
        self.className = className
        self.database = Database(className, sessionToken, language)
        self.sessionToken = sessionToken
        self.language = language
    
    def insert(self, item,returnJson=False ):
        return self.database.insert(item, returnJson)
        
    def get(self, itemId, query=None, returnJson=False):
        return self.database.get(itemId, query, returnJson)
        
    def getAll(self, query,returnJson=False):
        return self.database.getAll(query, returnJson)
    
    def update(self, itemId, item,returnJson=False):
        return self.database.update(itemId, item, returnJson)
        
    def delete(self, itemId, returnJson=False):
        return self.database.delete(itemId, returnJson)