var mainApp = angular.module("beerApp", ['ngMaterial', 'googlechart']);



mainApp.controller("beerController", function($scope, $http, $mdDialog, $location) {

	var urlBase = $location.$$absUrl
	var masterOperator = "beer"
	$scope.language = "pt"

	$scope.get = async function(modulo, filter){
		var url =`${urlBase}crud/${modulo}`;
		if(typeof filter != 'undefined')
        		url = url +"?"+filter
		var options = {
			    method: 'GET',
			    url: url,
			    headers: {'Content-Type': 'application/json', 'SessionToken':masterOperator, 'Language':$scope.language}
		}
		var result= await $http(options)
		return result.data.data
	};
	
	
	$scope.search = async function(){
	
		var textSearch = $scope.searchText.toUpperCase();
		if($scope.selectText == 'name' || $scope.selectText == 'gl' ){
			var filter = "filter_by="+$scope.selectText+"[eq]="+textSearch 
			$scope.beers = await $scope.get('beer', filter);
			$scope.$apply();
			
		}
		if($scope.selectText == 'harmonization' || $scope.selectText == 'ingredients'){
			var itemFilter = "filter_by=name[eq]="+textSearch
			var item =  await $scope.get('item', itemFilter);
			
			var filter = "filter_by="+$scope.selectText+"[in]="+item[0].objectId 
			$scope.beers = await $scope.get('beer', filter);
			$scope.$apply();	
		}
		
		if($scope.selectText == 'general'  ){
			var filter = "search_by="+textSearch 
			$scope.beers = await $scope.get('beer', filter);
			$scope.$apply();
			
		}
			
	}
	
	$scope.init = async function(){
        	$scope.beers = await $scope.get('beer')
        	$scope.$apply()
        
	}
	$scope.setLanguage = async function(language){
        	$scope.language = language
        	$scope.beers = await $scope.get('beer')
        	$scope.$apply();
        
	}
	
	
	$scope.init()
	
	


});



