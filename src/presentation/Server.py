#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 11:32:15 2018

@author: ailton
"""

from flask import Flask, request,flash,send_from_directory
from flask_restful import  Api
from flask_cors.extension import CORS
from flask_cors.decorator import cross_origin
from src.model.entity.EntityFactory import EntityFactory
from werkzeug.datastructures import ImmutableMultiDict


import src.utils.Properties as Properties
from werkzeug.utils import secure_filename
import os
import uuid

import traceback
import src.utils.Network as network
import json
import shutil
import base64


mainFolder  = Properties.getInstance().getMainFolder()
app = Flask(__name__, static_url_path='')
api = Api(app)
cors = CORS(app, resources={r"/*": {"origins": "*"}})
app.config['CORS_HEADERS'] = 'Content-Type'
UPLOAD_FOLDER = mainFolder+"src/presentation/static/uploads"
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

app.config['SESSION_TYPE'] = 'memcached'
app.config['SECRET_KEY'] = 'super secret key'



def getItem(data, form):
    if len(form) > 0:
        return form
    else:
        return json.loads(data)
    
    
def getSessionToken(header):
    if 'Sessiontoken' in header:
        return header['Sessiontoken']
    if 'SessionToken' in header:
        return header['SessionToken']
    return None

def getLanguage(header):
    print header
    if 'language' in header:
        return header['language']
    return 'pt'


def getAcessToken(header):
    if 'acessToken' in header:
        return header['acessToken'] == base64.b64encode('cachaceiro:cachaca')

     
"""From now the Flask API"""
"""Status the main server"""
@app.route('/')
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def root():
    return app.send_static_file('index.html')


@app.route('/status', methods=['GET'])
def status():
    return network.buildResponse(request.path, True, True )

"""Cachaceiro"""
@app.route('/crud/<objectClass>', methods=['POST','OPTIONS'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def crudPOST(objectClass):
    try:
        item = getItem(request.data, request.form)
        language = getLanguage(request.headers)
        sessionToken = getSessionToken(request.headers)
        entity = EntityFactory(objectClass, sessionToken, language)
        entityInstance = entity.getEntityInstance()
        itemSalvo = entityInstance.insert(item, True)
        response = network.buildResponse(request.path, True, itemSalvo)
        return response
    except:
        traceback.print_exc()
        errorMessage =traceback.format_exc(-1)
        response = network.buildErrorResponse(request.path,  errorMessage)
        return response


@app.route('/crud/<objectClass>/<objectId>', methods=['PUT','OPTIONS'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def crudPUT(objectClass, objectId):
    sessionToken = getSessionToken(request.headers)
    language = getLanguage(request.headers)
    entity = EntityFactory(objectClass, sessionToken, language)
    entityInstance = entity.getEntityInstance()
    item = json.loads(request.data)
    itemSalvo = entityInstance.update(objectId, item, True)
    response = network.buildResponse(request.path, True, itemSalvo)
    return response



@app.route('/crud/<objectClass>/<objectId>', methods=['GET','OPTIONS'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def crudGET(objectClass, objectId):
    sessionToken = getSessionToken(request.headers)
    language = getLanguage(request.headers)
    entity = EntityFactory(objectClass, sessionToken, language)
    entityInstance = entity.getEntityInstance()    
    isClassItem = objectClass == "item" and objectId.upper() in classType    
    if isClassItem:
        args = ImmutableMultiDict([("filter_by",u'classType[eq]='+objectId.upper() )])
        itens = entityInstance.getAll(args, True)
        response = network.buildResponse(request.path, True, itens) 
    else:
        itemSalvo = entityInstance.get(objectId, returnJson=True)
        response = network.buildResponse(request.path, True, itemSalvo)
    return response

@app.route('/crud/<objectClass>', methods=['GET','OPTIONS'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def crudGETALL(objectClass):
    sessionToken = getSessionToken(request.headers)
    language = getLanguage(request.headers)
    entity = EntityFactory(objectClass, sessionToken, language)
    entityInstance = entity.getEntityInstance()
    itens = entityInstance.getAll(request.args, True)
    response = network.buildResponse(request.path, True, itens)
    return response

@app.route('/crud/<objectClass>/<objectId>', methods=['DELETE','OPTIONS'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def crudDELETE(objectClass, objectId):
    sessionToken = getSessionToken(request.headers)
    language = getLanguage(request.headers)
    entity = EntityFactory(objectClass, sessionToken, language)
    entityInstance = entity.getEntityInstance()
    hasDeleted = entityInstance.delete(objectId)
    response = network.buildResponse(request.path, True, hasDeleted)
    return response

    


@app.route('/crud/user/login', methods=['POST','OPTIONS'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def login():
    try:       
        item = getItem(request.data, request.form)
        User = EntityFactory("user").getEntityInstance()

        if 'authData' in item:
            authData = item['authData']
            user = User.loginWithAuthData(authData,True)
        else:
            username = item['username']
            password = item['password']
            user = User.login(username, password,True)
        response = network.buildResponse(request.path, True, user)
        return response

    except:
        traceback.print_exc()
        errorMessage =traceback.format_exc(-1)
        response = network.buildErrorResponse(request.path,  errorMessage)
        return response
    

 
    

""" Start The server """
def run(port):
   hosts = network.getHostList()
   hostIndex = 1
   defaultHost = hosts[hostIndex]
    
   if network.isHostFree(defaultHost, port):
        print "Running server in "+ defaultHost + ":"+str(port)
        app.run(host=defaultHost, port=port , threaded=True)
   else:
        port = port+1
        print "Running server in "+ defaultHost + ":"+str(port)
        app.run(host=defaultHost, port=port , threaded=True)

        
        
        
        
        
       
    
