#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 13:51:08 2018

@author: ailton
"""

from src.data.Schema import Schema
import src.test.creator.Generator as generator
import json

class Builder:
    
    def __init__(self, className, request):
        self.className = className
        self.data = []
        self.schema = Schema(self.className)
        self.fields = self.schema.getFields()
        self.naoGerouFiltroDeCampo = True
        self.naoGerouFiltroDeFoto = True
        self.request = request
        
        
    def build(self):
        jsonObject = {}
        domains = self.schema.getFieldsAndDomains()
        for field in self.fields:
            jsonObject[field] = generator.generate(field, self.className, domains[field], self.request) 
        return jsonObject
    
    def buildList(self, size):
        return [self.build() for x in range(size)]
    
    
    def getFile(self):
        return generator.generateFile()
        
    
    def getFilter(self):
        vaiGerarFiltrosDeCampos = generator.generateBoolean() and self.naoGerouFiltroDeCampo
        if vaiGerarFiltrosDeCampos:
            self.naoGerouFiltroDeCampo = False
            generatedFileds =  generator.generateFilterArray(self.fields)
            generatedFiledsString = ""
            for field in generatedFileds:
                generatedFiledsString = generatedFiledsString +field+ ","
            
            return str("fields")+"="+str(generatedFiledsString[0: -1])
        else:
            return generator.generateFilter(self.className,self.data)
    
    def getObject(self):
        return generator.generateObject(self.data)
                                  
    def getFilters(self, howMany):
        filters = [self.getFilter() for x in range(howMany)]
        return filters
    
    
    def buildFilters(self, data):
        if len(data) > 3:
            self.data = data
        else:
            for x in range(3):
                item = self.build()
                endPoint = '/crud/'+self.className
                item = self.request.run(endPoint, "POST", post=json.dumps(item))
                self.data.append(item)
            
            
            
        
        
        
        