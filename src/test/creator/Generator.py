#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 13:50:54 2018

@author: ailton
"""
import os
import random
import json
import src.utils.Properties as properties
import string


schema = json.loads(open("src/data/parse/schema.json", 'r').read())['schema']
mainFolder = properties.getInstance().getMainFolder()
jsonFolder = "src/test/creator/json/"
fileFolder = "src/test/creator/files/"


    
valuesFields = {
    "price": [50, 500],
    "value": [1, 5],
    "aging":  [1, 36],
    "alcohol": [39, 44]   
}


filterArgs = {
    "barrel": ["filter_by=classType[eq]=BARREL"],
    "harmonization": ["filter_by=classType[eq]=INGREDIENT"],
    "sugarCaneSpecies": ["filter_by=classType[eq]=SUGARCANESPECIE"],
    "destilation": ["filter_by=classType[eq]=ALAMBIQUE"],
    "rewards": ["filter_by=classType[eq]=REWARD"],
    "leavening": ["filter_by=classType[eq]=LEAVENING"],
    "stamp": ["filter_by=classType[eq]=STAMP"]
}




def getObjects(className, request, field=None):
    endPoint = '/crud/'+className
    if field is not None and field in filterArgs:
        response = request.run(endPoint, "GET", args=filterArgs[field])
        return response['data']
    
    return request.run(endPoint, "GET", None)['data']


def generateRange(className,field):
    typeField =  schema[className]['fields'][field]
    itemOne = generate(field, className, typeField)
    itemTwo = generate(field, className, typeField)
    return 'filter_by='+str(field)+"[gte]="+str(min(itemOne, itemTwo)) +"," +str(field)+"[lte]="+str(max(itemOne, itemTwo))

def generateField(className, item):
    field = random.choice(item.keys())
    return field
   

def generateFilter(className, rawData):
    item = generateObject(rawData)
    field = generateField(className, item)
    value = item[field]
    domain = schema[className]['fields']
    willGenerateRange = field in domain and (domain[field]== "int" or domain[field] == "float"  ) and generateBoolean()
    if willGenerateRange:
        return generateRange(className,field)
    
    willGenerateArrayDependence = field in domain and type(domain[field]) is list and domain[field][0].startswith("pk_")
    if willGenerateArrayDependence:    
        random.choice(value)
        value1 = random.choice(value)['objectId']
        value2 = random.choice(value)['objectId']
        return 'filter_by='+str(field) + "[in]="+str(value1) + ","+str(field) + "[in]="+str(value2)

    willGenerateDependence = field in domain and domain[field].startswith("pk_")
    if willGenerateDependence:
        return 'filter_by='+str(field) + "[eq]="+str(value['objectId'])
    
    return 'filter_by='+str(field) + "[eq]="+str(value)

def generateObject(array):
    index = generateInt([0, len(array)-1])
    item = array[index]
    return item

def generateItemArray(array):
    index = generateInt([0, len(array)-1])
    item = array[index]
    return item
    

def generateFilterArray(array):
    arrayGenerated = []
    for item in array:
        vaiSerIncluso = generateBoolean()
        if vaiSerIncluso:
            arrayGenerated.append(item)
    if not arrayGenerated:
        item = random.choice(array)
        arrayGenerated.append(item)    
    return arrayGenerated



def generateFromJSON(itemType, field):
    fileName = itemType+"/"+field+".json"
    filePath = mainFolder+jsonFolder+fileName
    itens = json.loads(open(filePath, 'r').read())
    index = generateInt([0, len(itens)-1])
    return itens[index]
    

def generateBoolean():
    randomNumber = generateInt([0,9])
    return randomNumber%2 == 0


def generateFloat(domain):
    minNumber = -10000
    maxNumber = 10000
    if len(domain) > 1:
        minNumber = domain[0]
        maxNumber = domain[1]
    return round(random.uniform(minNumber,maxNumber), 2)



def generateInt(domain):
    minNumber = -10000
    maxNumber = 10000
    if len(domain) > 1:
        minNumber = domain[0]
        maxNumber = domain[1]
    return random.randint(minNumber,maxNumber)



def generateFile():
    randomFile = random.choice([x for x in os.listdir(fileFolder) if os.path.isfile(os.path.join(fileFolder, x))])
    return randomFile

def generateString():
    return ''.join(random.choice(string.ascii_uppercase) for _ in range(10))
def generateEmail():
    return generateString() + "@GMAIL.COM"

def generateArray(lambdaFunction, **args):
    arrayLength = generateInt([2,5])
    return [lambdaFunction(**args) for i in range(arrayLength)]

def generate(field, className, typeField, request=None):
    
    values = []
    if field in valuesFields:
        values = valuesFields[field]
        
    if type(typeField) is list and len(typeField) > 0:
        args = {
                "field": field,
                "className": className,
                "typeField": typeField[0],
                "request": request
                
        }
        return generateArray(generate,**args)
    
    if typeField.startswith("pk_"):
        objects = getObjects(typeField.split("pk_")[1].lower(), request,field)
        return generateItemArray(objects)
    
    if typeField.startswith("ln_"):
        objectField = {}
        objectField['pt'] = generateString()
        objectField['en'] = generateString()
        return objectField

    
    if typeField == "int":
        return generateInt(values)
    if typeField == "float":
        return generateFloat(values)
    if typeField == "bool":
        return generateBoolean()
    if typeField == "filter":
        return generateFilter(className)
    if typeField == "object":
        return generateObject(className)
    if typeField == "string":
        return generateString()
    if typeField == "email":
        return generateEmail()
    if typeField == "json":
        return generateFromJSON(className,field)

    
    
    