#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 15:57:47 2018

@author: ailton
"""


import requests


class Request:
    url = None
    port = 0
    host = None
    userName = None
    master = None
    DEVICE_SETTINGS = {
    'manufacturer'      : 'Xiaomi',
    'model'             : 'HM 1SW',
    'android_version'   : 18,
    'android_release'   : '4.3'
    }
    USER_AGENT = 'Instagram 9.2.0 Android ({android_version}/{android_release}; 320dpi; 720x1280; {manufacturer}; {model}; armani; qcom; en_US)'.format(**DEVICE_SETTINGS)


    def __init__(self, ip, port, url, userName = "None", master = "None"):
        '''
        Constructor
        '''
        self.LastJson = None
        self.ip = ip
        self.port = port
        self.url = url
        self.userName = userName
        self.master = master
        self.s = requests.Session()
        self.user = None
        self.sessionToken = None
        self.language = None


    def setUser(self,user):
        self.user = user
        self.sessionToken = user['sessionToken']
    
    def setLanguage(self,language):
        self.language = language
        

    def getPort(self):
        return self.port
        
    def run(self, route,method,args=None,post=None, files=None):
        url = ''.join([self.url , route])
        if args != None:
            url = url + "?"
            for arg in args:
                url = url + arg +"&"

            url = url[:-1]
        
        return self.sendRequest(url, method, post=post,files=files) 

   
        
    def sendRequest(self, url,method, post=None, files=None):
        
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}


        if self.sessionToken != None:
            headers['SessionToken'] = self.sessionToken
        
        if self.language != None:
            headers['language'] = self.language
        
        if method == 'POST': # POST
            response = self.s.post(url, headers = headers,data=post)
        if method == 'FORM':
            headers['Content-type'] = "application/x-www-form-urlencoded; charset=UTF-8"
            response = self.s.post(url, headers = headers,data=post)
        if method == 'GET': # GET
            response = self.s.get(url, headers = headers) # , verify=False
        
        if method == 'PUT': # PUT
            response = self.s.put(url, headers = headers,data=post)
            
        if method == 'PATCH': # PATCH
            response = self.s.patch(url, headers = headers,data=post)
        
        if method == 'DELETE': # PATCH
            response = self.s.delete(url, headers = headers,data=post)
        
        if method == 'FILE': # FILE
            session = requests.Session()
            if self.sessionToken != None:
                 session.headers['SessionToken'] = self.sessionToken
            if self.language != None:
                 session.headers['language'] = self.language
            response = session.post(url,files=files, data=post)           
       
        if response.status_code == 200:
            return response.json()
        else:
            try:
                errorResponse = {}
                errorResponse['error'] = response.json()
                errorResponse['code'] = response.status_code
                return errorResponse
            except:
                pass
            return False
    
        