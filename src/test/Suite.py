#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 14:30:19 2018

@author: ailton
"""


import unittest
from  src.test.serverTest.TestServer import  TestServer
from  src.test.serverTest.LoadData import  LoadData
from  src.test.serverTest.LoadDataFromCSV import  LoadDataFromCSV


from src.test.request.Request import Request
import src.utils.Network as network 
from src.test.creator.Builder import Builder
import json
import random
import src.utils.Properties as properties


mainFolder = properties.getInstance().getMainFolder()
jsonFolder = mainFolder+"src/test/creator/json/"
fileName = jsonFolder+"/user.json"
users = json.loads(open(fileName, 'r').read())



#List of modules that will be tested
testModules = {
        'beer': ['test01Create', 'test02GetWithFilters', 'test02GetAll','test03GetWithObjectId'],
        'user': ['test01Create', 'test05Login'],
        'item': ['test02GetWithFilters', 'test02GetAll','test03GetWithObjectId' ]
        
}



def buildRequest():
    indexOfServer = 1
    address = network.getHostList()[indexOfServer]
    request = Request(address, 8000, "http://"+ address +":" +str(8000))
    request.setLanguage("pt")
    return request

def buildRequestWithUser():
    request = buildRequest()
    user= random.choice(users)
    loginObject = {'username': user['username'], 'password': user['password']}
    endPoint = '/crud/user/login'
    loggedUser = request.run(endPoint, "POST", post=json.dumps(loginObject))
    request.setUser(loggedUser)
    return request


def loadDataFromCSV(runner):
    suite = unittest.TestSuite()
    request =  buildRequest()
    suite.addTest(LoadDataFromCSV("loadData", className='cachaca', request=request))
    runner.run(suite)


def loadData(runner):
    request =  buildRequest()
    data = ['item', 'user', 'beer']
    suite = unittest.TestSuite()
    for module in data:
        suite.addTest(LoadData("loadData", className=module, request=request))
    runner.run(suite)


def suite(runner):
    
    request =  buildRequestWithUser()
    for module in testModules:
        suite = unittest.TestSuite()
        print "\n===================================  Testing module " +module+ "  ===================================="
        testNames = testModules[module]
        builder = Builder(module, request)
        if module is not "user":
            endPoint = '/crud/'+module
            objects = request.run(endPoint, "GET", None)        
            builder.buildFilters(objects)
        
        for testName in testNames:
            suite.addTest(TestServer(testName, className=module, request=request, builder=builder))
        runner.run(suite)
        print "================================================================================================\n\n\n"

    
def load():
    runner = unittest.TextTestRunner(verbosity=2)
    loadData(runner)
    
 
def run():
    runner = unittest.TextTestRunner(verbosity=2)
    suite(runner)
    #loadDataFromCSV(runner)
    
 

if __name__ == '__main__':
    run()
