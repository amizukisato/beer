#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 14:40:35 2018

@author: ailton
"""

import unittest
import json
import src.utils.Properties as properties
from src.test.creator.Builder import Builder

mainFolder = properties.getInstance().getMainFolder()
jsonFolder = mainFolder+"src/test/creator/json/"




""" Test the class ParseComment """
class LoadData(unittest.TestCase):
    
    def __init__(self, methodName='runTest', className=None, request = None):
        super(LoadData, self).__init__(methodName)
        self.className = className
        self.request = request
        fileName = jsonFolder+"/"+self.className+".json"
        self.data = json.loads(open(fileName, 'r').read())
        
    def getArrayOfObjectId(self, jsonObject, field):
        objects = []
        for item in jsonObject[field]:
            objectJson = self.request.run('/crud/item', "GET", args=["filter_by=name[eq]="+item])
            objects.append({"objectId":objectJson['data'][0]['objectId']})
        return objects
            
        
        
   
    def loadData(self):
        #print self.data
        endPoint = '/crud/'+self.className
        for jsonObject in self.data:
            #atualiza dependencias
            if "harmonization" in jsonObject:
                jsonObject["harmonization"] = self.getArrayOfObjectId(jsonObject, "harmonization")
            if "ingredients" in jsonObject:
                jsonObject["ingredients"] = self.getArrayOfObjectId(jsonObject, "ingredients")
            response = self.request.run(endPoint, "POST", post=json.dumps(jsonObject))
                
               
    
    

    