#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 11 17:12:28 2018

@author: ailton
"""
import sys, getopt
import csv
import json

import unittest
import src.utils.Properties as properties
from src.test.creator.Builder import Builder
import yaml

mainFolder = properties.getInstance().getMainFolder()
folder = mainFolder+"src/test/creator/cachacas/"




""" Test the class ParseComment """
class LoadDataFromCSV(unittest.TestCase):
    
    def __init__(self, methodName='runTest', className=None, request = None):
        super(LoadDataFromCSV, self).__init__(methodName)
        self.className = className
        self.request = request
        csv_rows = []
        fileName = folder+"/cachacas.csv"
        with open(fileName) as csvfile:
            reader = csv.DictReader(csvfile)
            title = reader.fieldnames
            for row in reader:
                csv_rows.extend([{title[i]:row[title[i]] for i in range(len(title))}])
            self.jsonArray = yaml.safe_load(json.dumps(csv_rows)) 
        
    
   
    def loadData(self):
        #print self.data
        cachacaEndPoint = '/crud/'+self.className
        brandEndPoint = '/crud/brand'
        barrelEndPoint = "/crud/item"
        
        for jsonObject in self.jsonArray:            
            brandNameFilter = ["filter_by=name[eq]="+jsonObject['brand']]
            response = self.request.run(brandEndPoint, "GET", args=brandNameFilter)
            if len(response['data']) > 0:
                brand=response['data'][0] 
            else:
                brand={}
                brand['name'] = jsonObject['brand']
                brand['picture']=''
                brand = self.request.run(brandEndPoint, "POST", post=json.dumps(brand))
            
            barrels = []
            for madeira in jsonObject['barrel'].upper().split(","):
                madeira = madeira.strip()
                if madeira == 'MADEIRA DESCONHECIDA':
                    madeira = 'DESCONHECIDA'
                
                
                barrelFilterName = ["filter_by=name[eq]="+madeira]
                response = self.request.run(barrelEndPoint, "GET", args=barrelFilterName)
                barrels.append({
                    'objectId':  response['data'][0]['objectId']     
                })
            cachaca = jsonObject
            cachaca['barrel'] = barrels
            cachaca['brand'] = {'objectId': brand['objectId']}
            cachaca['curiosities'] = {
                    'en': '',
                    'pt':''
            }
            cachaca['rewards'] = []
            cachaca['harmonization']=[]
            cachaca['picture'] = ''
            cachaca['sugarCaneSpecies'] = []
            cachaca['leavening'] = {'objectId': "rUxeizx6Xb"}
            cachaca['stamp'] = []
            cachaca['destilation'] = {"objectId": "GBcfmcNq7S"}
            cachaca['linkBuy'] = ''
            cachaca['history'] = {
                    'en': '',
                    'pt':''
            }
            cachaca['aging'] = int(cachaca['aging'])
            cachaca['alcohol'] = float(cachaca['alcohol'])
            cachaca['price'] = float(cachaca['price'])



            response = self.request.run(cachacaEndPoint, "POST", post=json.dumps(cachaca))
            print response

            
            
    
    

    