#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 14:40:35 2018

@author: ailton
"""

import unittest
import json
import random

deniedFields = ['password']
def getField(item):
    field = random.choice(item.keys())
    if field not in deniedFields:
        return field
    else:
        return getField(item)
    
    
def getComparableItem(item, response ):
    
    try:
        field = getField(item)
        expected = item[field]
        current = response[field]
        if type(expected) is dict and "objectId" in expected:
            return (expected['objectId'], current['objectId'], field)
        if type(expected) is list:
            return (len(expected), len(current), field)
        return (expected, current, field)
 
    except:
        print field
        print expected
        print current
        return (True, True, field)
    
 

""" Test the class ParseComment """
class TestServer(unittest.TestCase):
    
    
    def __init__(self, methodName='runTest', className=None, request = None, builder = None):
        super(TestServer, self).__init__(methodName)
        self.className = className
        self.request = request
        self.builder= builder
        

    
   
    def test01Create(self):
        item = self.builder.build()
        endPoint = '/crud/'+self.className
        response = self.request.run(endPoint, "POST", post=json.dumps(item))
        (expected, current, field) = getComparableItem(item, response)
        self.assertEquals(expected, current, str(field) +" is incorrect.\n Expected "+ str(expected) + " to be equalt to "+ str(current))
        
        


    def test02GetAll(self):
        endPoint = '/crud/'+self.className
        response = self.request.run(endPoint, "GET")    
        #print json.dumps(response, indent=4, sort_keys=True)

    
    def test02GetWithSort(self):
        endPoint = '/crud/'+self.className
        filters = ["search_by=sex", "fields=name,tfTag,brand", "page=init=0,end=5"]
        response = self.request.run(endPoint, "GET", args=filters)    
        #print json.dumps(response, indent=4)
        
        
        
    
    def test02GetWithPagination(self):
        init = 0
        end = 2
        for x in range(5):
            filters = ["page=init="+str(init)+",end="+str(end)]
            endPoint = '/crud/'+self.className
            response = self.request.run(endPoint, "GET", args=filters)
            init = end
            end = end+2
            

        
    
    def test02GetWithFilters(self):
        endPoint = '/crud/'+self.className
        filters = self.builder.getFilters(3)
        response = self.request.run(endPoint, "GET", args=filters)        
        #print json.dumps(response, indent=4, sort_keys=True)


        
    def test03GetWithObjectId(self):
        item = self.builder.getObject()
        endPoint = '/crud/'+self.className+"/"+item['objectId']
        response = self.request.run(endPoint, "GET")
        field = "objectId"
        self.assertEquals(item[field], response[field], field +" is incorrect")

    def test04GetWithFile(self):
        fileFolder = "src/test/creator/files/"
        endPoint = '/crud/'+self.className+'/photo'
        fileName = self.builder.getFile()
        files = {'photo': open(fileFolder+fileName,'rb')}
        response = self.request.run(endPoint,"FILE", files=files, post={"limit": 3, 'minRate':0.03})
        print json.dumps(response, indent=4, sort_keys=True)
        
    def test05Login(self):
        userObject = self.builder.build()
        endPoint = '/crud/'+self.className
        self.request.run(endPoint, "POST", post=json.dumps(userObject))
        loginObject = {'username': userObject['email'], 'password': userObject['password']}
        endPoint = '/crud/'+self.className+"/login"
        response = self.request.run(endPoint, "POST", post=json.dumps(loginObject))
        self.assertIn('sessionToken', response, " There is no sessionToken")

    