#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 13:48:03 2018

@author: ailton
"""
import re

import src.data.Connector as connection 
connection.parseRegister()
from src.data.Schema import Schema
from parse_rest.datatypes import Object
from parse_rest.datatypes import ACL
from parse_rest.user import User
from parse_rest.connection import SessionToken
import src.utils.Properties as properties
config = properties.getInstance().getConfig()

"""Import parsers"""
from src.data.parse.QueryParser import QueryParser
from src.data.parse.FilterParser import FilterParser
from src.data.parse.FieldsParser import FieldsParser
from src.data.parse.DependenceParser import DependenceParser
from src.data.parse.ResultParser import ResultParser
from src.data.parse.PaginationParser import PaginationParser

tableWithPublicACL = ['Beer', 'Item']

def merge(array):   
    result = []
    objectIds = []
    for partialResult in array:
        for item in partialResult:
            #remove duplicates
            if item.objectId not in objectIds:
                objectIds.append(item.objectId)
                result.append(item)
    return result

def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z

class Database:
    
    def __init__(self, className, sessionToken,language):
        self.language = language
        self.sessionToken = sessionToken
        self.schema = Schema(className)
        self.table = self.schema.getTable()
        self.fieldsParse = FieldsParser()
        self.resultParser = ResultParser(self.schema, self.language)
        self.className = className
        """Global options"""
        self.limit = 100000
        self.skip = 0
    
    """ Auxiliar Methods """
    def getClass(self):
        className = self.table
        Class = Object.factory(className)
        return Class
    
    def convertJsonToObject(self, jsonObject):
        Class = self.getClass()
        classObject = Class()
        fields = self.schema.getFields()
        domains = self.schema.getFieldsAndDomains()
        for field in fields:
            domain = domains[field]
            #Is Pointer Array
            if type(domain) is list and domain[0].startswith("pk_"):
                pointerClass = domain[0].split("pk_")[1]
                pointers = []
                for item in jsonObject[field]:
                    pointer = {}
                    pointer['__type'] = "Pointer"
                    pointer['className'] = pointerClass
                    pointer['objectId'] = item['objectId']
                    pointers.append(pointer)
                setattr(classObject, field, pointers )
            #Is Pointer Atributte
            elif domain.startswith("pk_"):
                pointerClass = domain.split("pk_")[1]
                pointer = {}
                pointer['__type'] = "Pointer"
                pointer['className'] = pointerClass
                pointer['objectId'] = jsonObject[field]['objectId']
                setattr(classObject, field, pointer)
            #Is a language Attribute
            elif domain.startswith("ln_"):
                setattr(classObject, field, jsonObject[field])
                languages = self.schema.getSupportedLanguages()
                for language in languages:
                    if jsonObject[field] != None and language in jsonObject[field]:
                        setattr(classObject,"ln_"+language+"_"+field, jsonObject[field][language])
            else:
                setattr(classObject, field, jsonObject[field])
        return classObject
    
    
    
    def validade(self,jsonObject ):
        uniqueKeys = self.schema.getUniqueKeys()
        attributes = self.schema.getFieldsAndDomains()
        
        for key in uniqueKeys:
            domain = attributes[key]
            if domain.startswith('ln_'):
                supportedLanguages = self.schema.getSupportedLanguages()
                willRaiseError = True
                for language in supportedLanguages:
                    willRaiseError = willRaiseError and self.contains(key+"."+language,jsonObject[key][language] )
                if willRaiseError:
                    raise ValueError("Value Already exists _v_"+ key)
            else:
                if self.contains(key, jsonObject[key]):
                    raise ValueError("Value Already exists _v_"+ key)
        
    def getACL(self):
        if self.table in tableWithPublicACL:
            acl = ACL()
            acl.set_default(read=True, write = True)
            return acl
        else:
            with SessionToken(self.sessionToken):
                user = User.current_user()
                acl = ACL()
                acl.set_user(user, read=True, write=True)
                return acl
            
    def getUserId(self):
        with SessionToken(self.sessionToken):
            user = User.current_user()
            return user.objectId
        
    def getPermissionArgs(self, isFilter = False):
        permissionArgs = {}
        if self.sessionToken=='cachaceiro':
            return permissionArgs
        
        if self.table in tableWithPublicACL:
            permissionArgs['_rperm__in']=["*"];
        else:
            permissionArgs['_rperm__in']=["*",[self.getUserId()] ];
        
        if isFilter:
           permissionArgs['_rperm'] = { "$in":permissionArgs['_rperm__in'] } 
           del permissionArgs['_rperm__in'] 
        
        return permissionArgs
    
    """ CRUD """
        
    def insert(self, jsonObject, returnJson):
        try:
            classObject = self.convertJsonToObject(jsonObject)
        except:
            #print jsonObject
            raise 
            
        self.validade(jsonObject)
        if returnJson:
            return self.resultParser.parse(self.className, self.save(classObject))
        return self.save(classObject)
    
        
    def save(self, item ):
        item.ACL = self.getACL()
        item.save()
        return item
    
    def update(self, objectId, jsonObject, returnJson):
        classObject = self.get(objectId)
        for field in vars(jsonObject):
            setattr(classObject, field, jsonObject[field])
        if returnJson:
            return self.resultParser.parse(self.className, self.save(classObject))
        return self.save(classObject)
    
    
    def getAll(self, query, returnJson):
        Class = self.getClass()
        queryParser = QueryParser(query, self.schema.getFieldsAndDomains(), self.language)
        filters = queryParser.getFilters()
        fields = queryParser.getFields()
        searchQuery = queryParser.getSearchArgs()
        page =  PaginationParser(queryParser.getPagination())
        (init, end) = page.getArgs()
        ordenation = queryParser.getOrdenation()
        function=queryParser.getFunctions()
        query_results = []
        dependenceParser = DependenceParser(fields, self.schema.getFieldsAndDomains())
        dependenceArgs = dependenceParser.getQueryArgs()
        permissionArgs = self.getPermissionArgs()
        aggregateArgs = {}
        #query with search
        if searchQuery != None:
            query_results = Class.Query.filter(**searchQuery).order_by(ordenation).select_related(*dependenceArgs).skip(init).limit(end)
            aggregateArgs = searchQuery
        # Build Results with filters
        elif len(filters) > 0:
            query_results = []
            for simpleFilter in filters:
                filterParse = FilterParser(simpleFilter, self.schema.getFieldsAndDomains(), self.language)
                filterArgs = filterParse.getArgs()
                queryArgs = merge_two_dicts(filterArgs, self.getPermissionArgs(True))
                qResults = Class.Query.filter(**queryArgs).order_by(ordenation).select_related(*dependenceArgs).skip(init).limit(end)
                query_results.extend(qResults)
                aggregateArgs = queryArgs
        #Normal QUery
        else:
            aggregateArgs = permissionArgs
            query_results = Class.Query.filter(**permissionArgs).order_by(ordenation).select_related(*dependenceArgs).skip(init).limit(end)
        
        """ Build Results with fields """
        if len(fields) > 0:
            query_results = self.fieldsParse.get(fields, query_results)
            
        
        results = {}
        
        try:
            if len(function) > 0:
                results['aggregation'] = {}
                for f in function:
                    m = re.match(r'(.*)\[(.*)\]', f)
                    operation,field = (m.group(1), m.group(2))
                    
                    if operation == 'max':
                        operation_results =  Class.Query.filter().order_by("-"+field).limit(1)
                        operation_results =  operation_results[0] if len(operation_results) > 0 else {}
                        maxValue = getattr(operation_results,field)
                        results['aggregation']["max_"+field] = maxValue
                    if operation == 'min':
                        operation_results =  Class.Query.filter().order_by(field).limit(1)
                        operation_results =  operation_results[0] if len(operation_results) > 0 else {}
                        minValue = getattr(operation_results,field)
                        results['aggregation']["min_"+field] = minValue
                    if operation == 'sum':
                        sumResult = sum(getattr(result,field) for result in query_results)
                        results['aggregation']["sum_"+field] = round(sumResult,3)
                    if operation == 'count':
                        operation_results = Class.Query.filter(**aggregateArgs).count()
                        results['aggregation']["count"] = operation_results
                    if operation == 'avg':
                        if field == 'score':
                            ClassQuery = Object.factory("Score")
                            for item in query_results:
                                cachacaQuery = {
                                    "__type": 'Pointer',
                                    "objectId": item.objectId,
                                    "className": "Cachaca"
                                }
                                scores = ClassQuery.Query.filter(cachaca=cachacaQuery)
                                average = 0
                                if len(scores) > 0:
                                    sumResult = sum(getattr(result,'value') for result in scores)
                                    average = round((float(sumResult)/len(scores)),3)
                                setattr(item,'avg_score', average)
                                setattr(item,'count_score', len(scores))

                            
                        else:
                            average = 0
                            if len(query_results)>0:
                                sumResult = sum(getattr(result,field) for result in query_results)
                                average = round(sumResult/len(query_results),3)
                            results['aggregation']["avg_"+field] = average
                            
                        

        except :
            import traceback
            traceback.print_exc()
            pass
        
        if returnJson:
            query_results=self.resultParser.parseArray(self.className, query_results)
        
        results['data'] = query_results
    
        return results
         
            
        
    def get(self,objectId, query, returnJson):
        Class = self.getClass()
        permissionArgs = self.getPermissionArgs()
        permissionArgs['objectId'] = objectId
        itens = Class.Query.filter(**permissionArgs)
        if len(itens) > 0:
            if returnJson:
                return self.resultParser.parse(self.className, itens[0])
            return itens[0]
        return  {}
    
    
  
    def delete(self, objectId):
        with SessionToken(self.sessionToken):
            item = self.get(objectId)
            item.delete()
        
        
    
    def contains(self, field, value):
        Class = self.getClass()
        containsArgs = {field: value}
        permissionArgs = self.getPermissionArgs()
        queryArgs = merge_two_dicts(containsArgs, permissionArgs)
        retrievedObject = Class.Query.filter(**queryArgs).limit(1)
        return len(retrievedObject) > 0
    
    
        
        
        
    
    
        
        
