#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 13:48:39 2018

@author: ailton
"""

# -*- encoding: utf-8 -*-
# Made by http://www.elmalabarista.com
# This will teach about how use parse.com as your database

# About

# parse.com is a SAAS that provide a ready-to-use NOSQL backend
# and related services, great for quick prototypes. Also, can be used
# from several plataforms and languages

# Official documentation
# https://parse.com/docs/

"""
    These lines are for set the folder 'main' as the root.
    This will allow to import any files from this project
"""




import src.utils.Properties as properties

config = properties.getInstance().getConfig()



parseRestApiKey = config.get("Parse", "parseRestApiKey")
parseAplicationId = config.get("Parse", "parseAplicationId")
parseMasterKey = config.get("Parse", "parseMasterKey")
parseUrl = config.get("Parse", "parseUrl")






""" Set the environment (url base) from parser"""
import os
os.environ["PARSE_API_ROOT"] = parseUrl

from parse_rest.connection import register
from parse_rest.datatypes import Object



""" register into the parser """
def parseRegister(sessionToken = None):
    APPLICATION_ID = parseAplicationId
    REST_API_KEY = parseRestApiKey
    MASTER_KEY = parseMasterKey
    if sessionToken != None:
        register(APPLICATION_ID, REST_API_KEY, session_token=sessionToken)
    else:
        register(APPLICATION_ID, REST_API_KEY, master_key=MASTER_KEY)




def isConnected():
    
    try:
        className = "Connected"
        Connected = Object.factory(className)
        conneced = Connected.Query.get(objectId="KqgbFExlkI")
        isConnected =  conneced.connected
        return isConnected
         
    except:
        return False
    

    
    



