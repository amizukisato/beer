#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 10:52:16 2018

@author: ailton
"""

import src.utils.Properties as properties
config = properties.getInstance().getConfig()
from pymongo import MongoClient
database = MongoClient(config.get('Parse', 'mongourl'))
cachaceiro = database.cachaceiro
from src.model.entity.schema.Schema import Schema


class CreateIndex:
    
    def __init__(self):
        self.index = []
        
    def run(self):
        print "Creating Indexes"
        schema = Schema()
        schemas = schema.get()
        for key in schemas:
            try:
                item = schemas[key]
                table = item['table']
                if table != "_User":
                    indexes = []
                    fields = item['fields'].keys()
                    for field in fields:
                        domain = item['fields'][field]
                        if domain == "string":
                            indexes.append((field,"text" ))
                    collection = getattr(cachaceiro, table)
                    print collection.index_information()
                    collection.create_index(indexes)

            except:
                import traceback
                traceback.print_exc()
                pass
            
            

            
            
           
        