#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 17 10:09:27 2018

@author: ailton
"""

import src.utils.Properties as properties
config = properties.getInstance().getConfig()
from pymongo import MongoClient
database = MongoClient(config.get('Parse', 'mongourl'))
#cachaceiro = database.cachaceiro

class CleanDatabase:
    
    def __init__(self):
        self.index = []
        
    def run(self):
        print "Cleaning the Database"
        database.drop_database('cachaceiro')

