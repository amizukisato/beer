#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 23 13:58:54 2018

@author: ailton
"""
import traceback
from dateutil import parser
import re
import yaml
import ast
import json

class FilterParser:
    
    def __init__(self, filters, domains, language):
        self.parse(filters, domains, language)
        
    def parse(self, filters, domains, language):
        self.args = {}
        try:
            filterList = filters.split(",")
            for arg in filterList:
                field =  arg.split("[")
                operator = field[1].split("]=")[0]
                value =  field[1].split("]=")[1]
                field = field[0]
                isFloatValue = re.match("^\d+?\.\d+?$", value) is not None
                isDigit = value.isdigit()
                if isFloatValue:
                    value = float(value) 
                    value = round(value, 3)
                elif isDigit:
                    value= ast.literal_eval(value)
                
                if field == "_updated_at":
                    field = "updatedAt"
                    value= parser.parse(value)
                if field == "_created_at":
                    field = "createdAt"
                    value = parser.parse(value)
                
                if operator == "in":
                    if field in domains and type(domains[field]) is list and domains[field][0].startswith("pk_"):
                        domain = domains[field][0]
                        if field not in self.args:
                            self.args[field] = {'$in':[]}
                        self.args[field]['$in'].append({
                            "__type": 'Pointer',
                            "objectId": value,
                            "className": domain.split("pk_")[1]
                        })
                        
                
                elif operator == "eq":
                    if field in domains and type(domains[field]) is not list and domains[field].startswith("pk_"):
                        self.args[field] = {
                            "__type": 'Pointer',
                            "objectId": value,
                            "className": domains[field].split("pk_")[1]
                        }
                    elif field in domains  and domains[field].startswith("ln_"):
                        domain = domains[field]
                        self.args[field+"."+language] = value                                            
                    elif isFloatValue:
                        minValue = value - 0.01
                        maxValue = value + 0.01
                        self.args[field+"__gte"] = round(minValue, 3)
                        self.args[field+"__lte"] = round(maxValue, 3)
                    else:
                        self.args[field] = value
                        
                else:
                    self.args[field+"__"+operator] = value

        except:
            traceback.print_exc()            
    def getArgs(self):
            
        args = yaml.safe_load(json.dumps(self.args)) 
        return args
                