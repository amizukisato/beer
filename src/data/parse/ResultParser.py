#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 27 21:51:42 2018

@author: ailton
"""

import parse_rest.datatypes
from parse_rest.datatypes import ACL


class ResultParser:
    
    def __init__(self, schema,language):
        self.schema = schema
        self.language = language
        
    
    def parse(self,table,result):
        if type(result) is dict:
            return result
        dic = {}
        
        for field in vars(result):
            item =  getattr(result, field)
            typeItem = type(item)
            domain = self.schema.getDomain(table, field)
                
            if type(domain) is not list and typeItem is dict:
                if  domain.startswith("ln_") and self.language in item:
                    item = item[self.language]
                    
            if typeItem is list:
                tableDomain = table
                if domain[0].startswith("pk_"):
                    tableDomain = domain[0].split("pk_")[1]
                dic[field] = self.parseArray(tableDomain,item)
            elif typeItem is parse_rest.datatypes.Date:
                s = vars(item)
                dic[field] = s['_date'].isoformat()
            elif typeItem is ACL or field.startswith("ln_"):
                #dic[field] = vars(item)['_acl']
                pass
            elif typeItem.__module__ == 'parse_rest.datatypes':
                dic[field] =  self.parse(domain,item)      
            else:
                dic[field] = item
                
        return dic
        
        
    def parseArray(self,table,results):
        jsonArray = []
        for result in results:
            jsonArray.append(self.parse(table,result))
        return jsonArray
            
        