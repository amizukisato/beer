#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 23 12:41:22 2018

@author: ailton
"""

parser_params = ['filter_by', 'sort_by', 'fields']



class QueryParser:
    
    def __init__(self, query, domains, language):
        self.query = query
        self.filters = {}
        self.domains = domains
        self.ordenation = '-createdAt'
        self.language = language
        self.searchFields = []
        self.function=[]
        self.parse()
        
    def parse(self):
        self.filters = self.query.getlist('filter_by')
        self.fields = self.query.getlist('fields')
        self.pagination = self.query.getlist('page')
        self.searchFields = self.query.getlist('search_by')
        ordenation = self.query.getlist('sort_by')
        self.function=self.query.getlist('function')
        if len(ordenation) > 0:
            ordenation = ordenation[0]
            isDecreasing = ordenation.startswith("-")
            field = ordenation[1:] if isDecreasing  else ordenation
            if field in self.domains and self.domains[field].startswith("ln_"):
                if isDecreasing:
                    self.ordenation = "-ln_"+self.language+"_"+field
                else:
                    self.ordenation = "ln_"+self.language+"_"+field

            else:
                self.ordenation = ordenation
      
    def getFilters(self):
        return self.filters
    
    def getFields(self):
        if len(self.fields) > 0:
            self.fields = self.fields[0].split(",")
        return self.fields
    
    def getPagination(self):
        return self.pagination
    
    def getOrdenation(self):
        return self.ordenation
    
    def getFunctions(self):
        return self.function
    
    def getSearchArgs(self):
        if len(self.searchFields) > 0:
            searchArgs = {'$or':[]}
            term = self.searchFields[0]
            for field in self.domains:
                domain = self.domains[field]
                if domain == "ln_string" or domain == "string":
                    searchArgs['$or'].append({field: { "$regex": term }})
            return searchArgs
        else:
            return None
            
        
    
       
        
    
    