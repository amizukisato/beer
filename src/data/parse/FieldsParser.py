#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 23 14:06:39 2018

@author: ailton
"""

class FieldsParser:
    
    def get(self, fields, results):
        # for convenience objectId is required
        if "objectId" not in fields:
            fields.append("objectId")

        for item in results:
            current_fields = vars(item)
            for field in list(current_fields):
                if field not in fields:
                    delattr(item, field)
        return results