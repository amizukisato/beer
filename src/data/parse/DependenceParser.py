#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 24 17:30:44 2018

@author: ailton
"""

        
class DependenceParser:
    
    def __init__(self, fields, domains):
        
        if len(fields) == 0:
            self.fields = domains.keys()
        else:
            self.fields = fields
        self.domains = domains
        self.queryArgs = {}
        self.parse()    
    
    
    
    def parse(self):
        selecetedArgs = [] 
        for field in self.fields:
            if field in self.domains:
                domain = self.domains[field]
                if type(domain) is list and domain[0].startswith("pk_"):
                    selecetedArgs.append(field) 
                elif domain.startswith("pk_"):
                    selecetedArgs.append(field) 
                
        if len(selecetedArgs) > 0:
            self.queryArgs = selecetedArgs
        
        
        
        
        
    def getQueryArgs(self):
        return self.queryArgs
        
        