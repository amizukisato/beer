#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 09:59:14 2018

@author: ailton
"""

class PaginationParser:
    
    def __init__(self,pagination):
        self.init = 0
        self.limit = 1000
        self.parse(pagination)

        
    def parse(self, pagination):
        try:
            if pagination == None or len(pagination) == 0:
                return
            pagination = pagination[0].split(",")
            try:
                self.init = pagination[0].split("init=")[1]
            except:
                pass
            try:
                self.limit = pagination[1].split("end=")[1]
            except:
                pass
        except:
            pass
        
        
    def getArgs(self):
        end = int(self.limit) - int(self.init)
        return (self.init, end)
        