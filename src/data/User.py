#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 20 16:23:59 2018

@author: ailton
"""
from src.data.Schema import Schema

from parse_rest.user import User
from parse_rest.datatypes import Object
from parse_rest.connection import SessionToken
from src.data.parse.ResultParser import ResultParser


class UserData:
    
    
    def __init__(self, language):
        self.className = "user"
        self.schema = Schema(self.className)
        self.table = self.schema.getTable()
        self.language = language
        self.resultParser = ResultParser(self.schema, self.language)
        """Global options"""
        self.limit = 100000
        self.skip = 0
        
        
    def login(self, username, userpassword, returnJson):
        user = User.login(username, userpassword)
        if returnJson:
            return self.resultParser.parse(self.className,user)
        return user
    
    def loginWithAuthData(self,authData, retornJSON):
        user = User.login_auth(authData)
        if retornJSON:
            return self.resultParser.parse(self.className,user)
        return user
    
    def get(self, userId, returnJson):
        className = "User"
        UserClass = Object.factory(className)
        user = UserClass.Query.filter(objectId=userId).limit(1)[0]
        return  user
    
    def getBySessionToken(self, sessionToken,returnJson):
        with SessionToken(self.sessionToken):
            user = User.current_user()
            if returnJson:
                return self.resultParser.parse(self.className,user)
            return user

        
        
    def register(self, userData, returnJson):
        userName = userData['username']
        userEmail = userData['email']
        userPassword=userData['password']
        name = userData['name']
        user = User.signup(userName,userPassword , name=name, email=userEmail)
        user.email = userEmail
        user.name = name
        if returnJson:
            return self.resultParser.parse(self.className,user)
        return user
    
    
    
    