#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 13:47:17 2018

@author: ailton
"""

import json
import src.utils.Properties as properties

class Schema:
    
    
    def __init__(self,className):
        mainFolder = properties.getInstance().getMainFolder()
        schemaFileName = mainFolder + 'src/data/parse/schema.json'
        schemaFile = open(schemaFileName, 'r').read()
        self.schema = json.loads(schemaFile)['schema']
        self.className = className
        if self.className not in self.schema:
            raise ValueError( "Schema does not exists for  "+ className)
        
    
    def getFields(self):
        selectedSchema = self.schema[self.className]
        fields = selectedSchema['fields'].keys()
        return fields
    
    def getFieldsAndDomains(self):
        selectedSchema = self.schema[self.className]
        fields = selectedSchema['fields']
        return fields
    def getUniqueKeys(self):
        selectedSchema = self.schema[self.className]
        if 'unique' in selectedSchema:
            return selectedSchema['unique']
        return []
    
    def getDomain(self, table,field ):
        table = table[0].lower() +table[1:]
        selectedSchema = self.schema[table]
        domains = selectedSchema['fields']
        if field in domains:
            domain = domains[field]
            if type(domain) is not list and domain.startswith("pk_"):
                return domain.split("pk_")[1]
            return domain
        return ""
    
    def getSupportedLanguages(self):
        return ['en', 'pt']
    
        
    def getTable(self):
        selectedSchema = self.schema[self.className]
        return selectedSchema['table']
    
    def getSchema(self):
        return self.schema
    
