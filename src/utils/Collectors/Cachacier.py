#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 14 09:01:20 2018

@author: ailton
"""
# encoding=utf8

import sys
reload(sys)
sys.setdefaultencoding('utf8')

import src.utils.Properties as properties
mainFolder = properties.getInstance().getMainFolder()

from src.test.request.Request import Request
import json
import src.utils.Network as network 
import re

import csv

class CachacierCollector:
    
    def __init__(self):
          #self.request = Request("cachacie.ioasys.com.br/api", 80, "http://cachacie.ioasys.com.br/api")
        indexOfServer = 1
        address = network.getHostList()[indexOfServer]
        request = Request(address, 8000, "http://"+ address +":" +str(8000))
        request.setLanguage("pt")
        self.request = request
             
    def convertCachaca(self, cachacaParaConverter):
        cachacaEndPoint = '/crud/cachaca'
        brandEndPoint = '/crud/brand'
        barrelEndPoint = "/crud/item"
        brandName= cachacaParaConverter['fabricante'].replace(",", "").replace("&", "").strip()
        
        brandNameFilter = ["filter_by=name[eq]="+brandName]
        response = self.request.run(brandEndPoint, "GET", args=brandNameFilter)
        if len(response['data']) > 0:
            brand=response['data'][0]
            
        else:
            brand={}
            brand['name'] =brandName
            brand['picture']=''
            brand = self.request.run(brandEndPoint, "POST", post=json.dumps(brand))
        
        barrels = []
        madeiras = cachacaParaConverter['tipoMadeira'].upper().split(" E ")
        madeirasExtras = []
        madeirasExtras2 = []
        for madeira in madeiras:
            if madeira != '' and len( madeira.split(','))>0 :
                madeirasExtras.extend(madeira.split(','))
                madeiras.remove(madeira)
        
        for madeira in madeiras:
            if madeira != '' and len( madeira.split('-'))>0 :
                madeirasExtras2.extend(madeira.split('-'))
                madeiras.remove(madeira)
        
        madeiras.extend(madeirasExtras)
        madeiras.extend(madeirasExtras2)

        #print madeiras

        for madeira in madeiras:
            madeira = madeira.strip()
            if madeira == 'MADEIRA DESCONHECIDA' or madeira == '' or madeira == 'NEUTRA' or madeira == "COMPOSTA":
                madeira = 'DESCONHECIDA'
            if madeira == 'GÁPIA':
                madeira = "GRÁPIA"
            if madeira == 'UMBURANA' or madeira == 'AMBURANAU':
                madeira = "AMBURANA"
            if madeira == "AMINDOIM":
                madeira = "AMENDOIM"
            if madeira == "NÃO PASSA" or madeira == "SEM MADEIRA":
                madeira = "DESCONHECIDA"
            if madeira == "CASTANHEIRA DO PARÁ":
                madeira = "CASTANHA DO PARÁ"
            if madeira == "IPÊ BRANCO" or madeira == "YPÊ AMARELO":
                madeira = "IPÊ"
            if madeira == "AÇO INOX":
                madeira = "INOX"
            if madeira == "MADEIRA DE OLIVA":
                madeira = "OLIVEIRA"
            if madeira == "AMERICANO":
                madeira =  "CARVALHO AMERICANO"
            if madeira == "5 ANOS EM CARVALHO FRANCÊS":
                madeira = "CARVALHO FRANCÊS"
            if madeira == "1 ANO EM BÁLSAMO":
                madeira = "BÁLSAMO"
            if madeira == "CANELA SASSAFRÁS":
                madeira = "CANELA"
            if madeira == "BLEND DE CARVALHOS":
                madeira = "CARVALHO"
            if madeira == "CAVALHO FRANCÊS":
                madeira = "CARVALHO"
            if madeira ==  "CABRIÚNA" or madeira == 'CABRIÚVA':
                madeira = "CABREÚVA"            
                
            try:
                barrelFilterName = ["filter_by=name[eq]="+madeira]
                response = self.request.run(barrelEndPoint, "GET", args=barrelFilterName)
                barrels.append({
                        'objectId':  response['data'][0]['objectId']     
                        })
            except:
                print madeira
        cachaca = {}
        cachaca['barrel'] = barrels
        cachaca['brand'] = {'objectId': brand['objectId']}
        cachaca['curiosities'] = {
                'en': '',
                'pt':''
        }
        cachaca['rewards'] = []
        cachaca['harmonization']=[]
        cachaca['picture'] = ''
        cachaca['sugarCaneSpecies'] = []
        cachaca['leavening'] = {'objectId': "rUxeizx6Xb"}
        cachaca['stamp'] = []
        cachaca['destilation'] = {"objectId": "GBcfmcNq7S"}
        cachaca['linkBuy'] = ''
        cachaca['history'] = {
                'en': '',
                'pt':''
        }
        cachaca['aging'] = 0
        try:
            cachaca['alcohol'] =float(re.findall(r'[\d\.\d]+', str(cachacaParaConverter['graduacaoAlcoolica']))[0])
        except:
            cachaca['alcohol'] = 0
        try:
            cachaca['price'] = float(cachacaParaConverter['precoMedio'])
        except:
             cachaca['price'] = 0
        
        cachaca['name'] = cachacaParaConverter['nome']
        
        return cachaca
        
        
    
        
    def start(self):
        """
        endPoint = "/pesquisarcachacaweb"
        for x in range(1,100):
           response = self.request.run(endPoint, "GET", args=["numero="+str(x)])
           cachacas = response['data']
           with open('cachacas'+str(x)+'.json', 'w') as outfile:
                json.dump(cachacas, outfile)
        """
        cachacaEndPoint = '/crud/cachaca'

        
        folder = mainFolder+"cachacas/"
        cachacas = []
        for x in range(1,95):
           fileName = folder+'cachacas'+str(x)+'.json'
            
           with open(fileName, 'r') as cachacasFile:
               cachacaRead = json.load(cachacasFile)
               cachacas.extend(cachacaRead)

        for cachaca in cachacas:
            #try:
            #print cachaca    
            cachacaConvertida =  self.convertCachaca(cachaca)
            print cachacaConvertida
            print cachaca
            #self.request.run(cachacaEndPoint, "POST", post=json.dumps(cachacaConvertida))
        
                #print cachaca
                
                
                



        
        