# -*- coding: utf-8 -*- 

'''
Created on 6 de nov de 2016

@author: ailton
'''

from netifaces import interfaces, ifaddresses, AF_INET
from contextlib import closing
from flask import jsonify
import socket
import parse_rest.datatypes
from parse_rest.datatypes import ACL
 

"""  HOST METHODS """
def getHostList():
    serverHosts = []
    for ifaceName in interfaces():
        addresses = [i['addr'] for i in ifaddresses(ifaceName).setdefault(AF_INET, [{'addr':'No IP addr'}] )]
        for address in addresses: 
            if not str(address).startswith('No'):
                serverHosts.append( address)
                
    return serverHosts            
    

   
def isHostFree(serverHost,serverPort ):
    connectionStatus = False
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
        if sock.connect_ex((serverHost, serverPort)) == 0:
            print "Port in use"
        else:
            print "Port not in use"
            connectionStatus = True
    return connectionStatus 



""" Parse server methods """
def getError(message):

    mapError = {
        "Invalid username/password": {
               "message": {
                       'pt': "Usuario/Senha invalidos",
                       'en': "Invalid username/password"
                } , 
               "code": 422
        },
        "The photo was not recognized":{
                "message": {
                       'en': "The picture was not recognized in our system",
                       'pt': "A foto nao pode ser reconhecida."
                }, 
               "code": 423
        
        },
        "ValueError: Value Already exists":{
                "message": {
                       'en': "This entity already exists, please check the 'name' atribute. It's unique",
                       'pt': "A entidade já existe, verifique o atributo 'name' que é único."
                },
               "code": 409
        }
    }
    
    keys = mapError.keys()
    for key in keys:
        if key in message:
            
            
            return mapError[key]

    return {
            "message":{
                    "pt": "Erro Interno do Servidor",
                    "en": "Server Internal Error",
                    "def": message
             },
            "code": 500
    }   
    


def make_json_response(reponseObject, code):
    response = jsonify(reponseObject)
    response.status_code = code
    return response

def buildErrorResponse(path,  errorMessage):
    error = getError(errorMessage)
    return make_json_response(error['message'], error['code'])


def buildResponse(path, status, data=[], code = 500, serverMessage=None):
    if serverMessage:
        return make_json_response(serverMessage, code)
    
    if status:
        return make_json_response(data, 200)
    else:
        return make_json_response(data, code)
    
    
    



def convertParseArrayToJson(parseArray):
    jsonArray = []
    for parseObject in parseArray:
        jsonArray.append(convertParseObjectToJson(parseObject))
    return jsonArray


    
def convertParseObjectToJson(parseObject):
        
        if type(parseObject) is dict:
            return parseObject
        dic = {}
        
        for field in vars(parseObject):
            item =  getattr(parseObject, field)
            typeItem = type(item)
            
            if typeItem is list:
                dic[field] = convertParseArrayToJson(item)
            elif typeItem is parse_rest.datatypes.Date:
                s = vars(item)
                dic[field] = s['_date'].isoformat()
            elif typeItem is ACL:
                #dic[field] = vars(item)['_acl']
                pass
            
            elif typeItem.__module__ == 'parse_rest.datatypes':
                dic[field] =  convertParseObjectToJson(getattr(parseObject, field))      
            else:
                dic[field] = item
                
        
        
        return dic
        


def fillParseObject(parseObject, jsonObject, fields ):
    for field in fields:
        if field in jsonObject:
            setattr(parseObject, field, jsonObject[field])
    return parseObject
    
 
    