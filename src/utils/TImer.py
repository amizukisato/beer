#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 11:50:06 2018

@author: ailton
"""


def timeToString(timeToParse):
    
    stringTime = ""
    
    timeToParse = int(timeToParse)
    
    if timeToParse > 86400:
        value = timeToParse//86400
        stringTime = str(value)+"D "
        timeToParse = timeToParse - value*86400
        
    if timeToParse > 3600:
        value = timeToParse//3600
        stringTime = stringTime + str(value)+"H "
        timeToParse = timeToParse - value*3600
        
    if timeToParse > 60:
        value = timeToParse//60
        stringTime = stringTime + str(value)+"M "
        timeToParse = timeToParse - value*60
        
    if timeToParse > 0:
        value = timeToParse
        stringTime = stringTime + str(value)+"S "
        
    return stringTime

def readTime(timeString):
    
    r = re.compile("([0-9]+)([a-zA-Z]+)")
    m = r.match(timeString)
    timeFormat = m.group(2).upper()
    if( timeFormat == "H"):
        return int(m.group(1))*3600
    if( timeFormat == "D"):
        return int(m.group(1))*86400
    if( timeFormat == "M"):
        return int(m.group(1))*60
    if( timeFormat == "S"):
        return int(m.group(1))*1
    
    return 1