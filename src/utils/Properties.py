'''
Created on 7 de dez de 2016

@author: ailton
'''

import sys
mainFolder  = sys.path[0]
import ConfigParser
config = ConfigParser.ConfigParser()
config.read(mainFolder+'config/config.properties')
privateInstance = None


def getInstance():
    global privateInstance
    if privateInstance == None:
        privateInstance = Properties()
    return privateInstance
    


class Properties:
    
    def __init__(self):
        self.properties = config
        self.mainFolder = mainFolder
        
    def getConfig(self):
        return self.properties
    
    def getMainFolder(self):
        return self.mainFolder
    
    
    
    
    
    