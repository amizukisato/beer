'''
Created on 6 de dez de 2016

@author: ailton
'''


""" Change the terminal colors"""
class Colors:
    HEADER =    '\033[95m'
    OKBLUE =    '\033[94m'
    OKGREEN =   '\033[92m'
    WARNING =   '\033[93m'
    FAIL =      '\033[91m'
    ENDC =      '\033[0m'
    BOLD =      '\033[1m'
    UNDERLINE = '\033[4m'
    
    BLACK =      "\033[0;37;40m \033[0m"
    RED =        "\033[0;37;41m \033[0m"
    GREEN =      "\033[0;37;42m \033[0m"
    YELLOW =     "\033[0;37;43m \033[0m"
    BLUE =       "\033[0;37;44m \033[0m"
    PURPLE =     "\033[0;37;45m \033[0m"
    BBLUE =      "\033[0;37;46m \033[0m"
    CYAN =       "\033[0;37;47m \033[0m"
    DGRAY =      "\033[0;37;100m \033[0m"
    ORANGE =     "\033[30;48;5;202m \033[0m"
    PINK =       "\033[30;48;5;207m \033[0m"
    BROWN =      "\033[30;48;5;94m \033[0m"
    
    
    BLACKHASH =  "\033[0;37;40m#\033[0m"
    REDHASH  =   "\033[0;37;41m#\033[0m"
    GREENHASH =  "\033[0;37;42m#\033[0m"
    YELLOWHASH = "\033[0;37;43m#\033[0m"
    BLUEHASH =   "\033[0;37;44m#\033[0m"
    PURPLEHASH = "\033[0;37;45m#\033[0m"
    BBLUEHASH =  "\033[0;37;46m#\033[0m"