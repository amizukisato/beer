#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 23 13:31:17 2018

@author: ailton
"""

import json,httplib,urllib
import Properties as properties
import Network as network
config = properties.getInstance().getConfig()



def getUserBySessionToken(sessionToken):
    
    host = network.getHostList()[1] 
    port = config.get("Parse", "parsePort")
    apiKey = config.get("Parse", "parseAplicationId")
    restKey =  config.get("Parse", "parseRestApiKey")
    masterKey = config.get("Parse", "parseMasterKey")

    
    connection = httplib.HTTPConnection(host, port)
    params = urllib.urlencode({"where":json.dumps({
       "sessionToken": sessionToken,
    })})
    connection.connect()
    connection.request('GET', '/parse/sessions?%s'%params, '', {
           "X-Parse-Application-Id": apiKey,
           "X-Parse-REST-API-Key": restKey,
           "X-Parse-Master-Key": masterKey
           
    })
    result = json.loads(connection.getresponse().read())
    return  result['results'][0]['user']['objectId']
